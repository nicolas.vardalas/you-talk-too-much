import Intervention from '@/models/intervention';
import Meeting, { Talker } from '@/models/meeting';
import Gender from '@/models/gender';

describe('Meeting', () => {
  const interventions = [
    new Intervention(0, 2, 'A', false, null, false, 'B'),
    new Intervention(3, 5, 'B', false, 'A', true, 'A'),
    new Intervention(4.5, 7, 'A', true, 'B', false, 'C'),
    new Intervention(7, 10, 'C', false, 'C', false, null),
  ];
  const meeting = new Meeting(interventions);

  it('duration', () => {
    expect(meeting.duration).toEqual(10);
  });
  it('talkers', () => {
    const expected = {
      A: new Talker('A', 0),
      B: new Talker('B', 1),
      C: new Talker('C', 2),
    };
    expect(meeting.talkers).toEqual(expected);
  });
  it('talkerTotalContribution', () => {
    const expected = {
      A: 4.5,
      B: 2,
      C: 3,
    };
    expect(meeting.talkerTotalContribution()).toEqual(expected);
  });

  describe('genderContributionRatio', () => {
    const genderInterventions = [
      new Intervention(0, 2, 'A', false, null, false, 'B'),
      new Intervention(2, 3, 'B', false, 'A', true, 'A'),
      new Intervention(3, 5, 'A', true, 'B', false, 'C'),
      new Intervention(5, 7, 'C', false, 'A', false, 'A'),
      new Intervention(7, 9, 'A', false, 'C', false, 'D'),
      new Intervention(9, 12, 'D', false, 'A', false, null),
    ];

    describe('no talker has gender', () => {
      const genderedMeeting = new Meeting(genderInterventions);

      it('isGenderFullyAttributed', () => {
        const got = genderedMeeting.isGenderFullyAttributed();

        expect(got).toBe(false);
      });

      it('genderContributionRatio', () => {
        const got = genderedMeeting.genderContributionRatio();

        expect(got).toEqual({
          undefined: 1,
        });
      });
    });

    describe('some talker do not have gender', () => {
      const genderedMeeting = new Meeting(genderInterventions);
      genderedMeeting.setTalkerGender('A', Gender.FEMALE);
      genderedMeeting.setTalkerGender('B', Gender.MALE);
      genderedMeeting.setTalkerGender('C', Gender.NON_BINARY);

      it('isGenderFullyAttributed', () => {
        const got = genderedMeeting.isGenderFullyAttributed();

        expect(got).toBe(false);
      });

      it('genderContributionRatio', () => {
        const got = genderedMeeting.genderContributionRatio();

        expect(got[Gender.FEMALE]).toBeCloseTo(0.5);
        expect(got[Gender.NON_BINARY]).toBeCloseTo(2.0 / 12);
        expect(got[Gender.MALE]).toBeCloseTo(1.0 / 12);
        expect(got.undefined).toBeCloseTo(0.25);
      });
    });

    describe('talker all have gender', () => {
      const genderedMeeting = new Meeting(genderInterventions);
      genderedMeeting.setTalkerGender('A', Gender.FEMALE);
      genderedMeeting.setTalkerGender('B', Gender.MALE);
      genderedMeeting.setTalkerGender('C', Gender.NON_BINARY);
      genderedMeeting.setTalkerGender('D', Gender.FEMALE);

      it('isGenderFullyAttributed', () => {
        const got = genderedMeeting.isGenderFullyAttributed();

        expect(got).toBe(true);
      });

      it('genderContributionRatio', () => {
        const got = genderedMeeting.genderContributionRatio();
        expect(got[Gender.FEMALE]).toBeCloseTo(((9 / 12) / 2) * (8 / 5));
        expect(got[Gender.NON_BINARY]).toBeCloseTo((2 / 12) * (8 / 5));
        expect(got[Gender.MALE]).toBeCloseTo((1 / 12) * (8 / 5));
      });
    });
  });
});
