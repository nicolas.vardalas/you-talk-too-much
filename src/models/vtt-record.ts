export class VttRecord {
  readonly timeStart: number;

  readonly timeEnd: number;

  readonly talker: string;

  constructor(timeStart: number, timeEnd: number, talker: string) {
    this.timeStart = timeStart;
    this.timeEnd = timeEnd;
    this.talker = talker;
  }
}

export class VttCollection {
  readonly list:VttRecord[]=[];

  append(record:VttRecord):void {
    this.list.push(record);
  }
}
